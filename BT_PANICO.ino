#include <SoftwareSerial.h> 
SoftwareSerial miBT(10, 11);
int myHalt=0;
const int LED = 52;
const int HALT = 9;
void setup(){
  Serial.begin(9600);
  miBT.begin(9600);
  pinMode(LED,OUTPUT);
  pinMode(HALT,INPUT);
  digitalWrite(HALT,LOW);
  digitalWrite(LED,LOW);
  Serial.println("Ready");
}
void loop(){
int i;
if (miBT.available())       // si hay informacion disponible desde modulo
{
   Serial.write("ALARMA\n");   // lee Bluetooth y envia a monitor serial de Arduino
   while(!myHalt)
   {
      digitalWrite(LED,HIGH);
      delay(1000);
      for(i=0;i<30;i++)
      {
      digitalWrite(LED,LOW);
      delay(100);
      digitalWrite(LED,HIGH);
      delay(100);
      }
      myHalt = 0; // Loop
      if (digitalRead(HALT) == HIGH) {
        myHalt = 1;
      }
   }
   myHalt=0;
   digitalWrite(LED,LOW);
   delay(500);
   while(miBT.available())
   {
      miBT.read();
   }
}
if (Serial.available())     // si hay informacion disponible desde el monitor serial
   miBT.write(Serial.read());   // lee monitor serial y envia a Bluetooth

}
