#include <SoftwareSerial.h> 
#include <TimerOne.h>
//-------------------------------------------------//
//      V A R I A B L E S :
//-------------------------------------------------//
//  PIN 10 -> RX
//  PIN 11 -> TX
SoftwareSerial miBT(10, 11);

//#define ALARMA_CERCA
#define ALARMA_LEJOS

int global_cnt = 0;
int global_dne = 0;
int global_cfg = 0;
int cont_nodet = 0;

int i;
int al_flag = 0;

char global_char;

char key[20] = "+INQ:2016:2:258506,0,";
char string[30];
char rssi[4];

int rssi_flag = 0;
int rssi_value;
int pointer_tok;
int interrupt_close = 0;
int noclose_flag = 0;

const int LED = 4;
int CYCLES_ALARM = 2;
//-------------------------------------------------//
//      R U T I N A S :
//-------------------------------------------------//
void Ala() {
//  digitalWrite(LED,HIGH);
    al_flag = 1;
}
void closeAla() {
//  digitalWrite(LED,LOW);
    al_flag = 0;
}
void _Donce_() {
  if (global_cnt == 1) {
    Serial.println("Said: AT\n");
    miBT.println("AT\n");
    pointer_tok = 0;
  }  
  if (global_cnt == 2) {
    Serial.println("Said: AT\n");
    miBT.println("AT\n");
    pointer_tok = 0;
  }
  if (global_cnt == 3) {
    Serial.println("Said: AT+PIO=11,1\n");
    miBT.println("AT+PIO=11,1\n");
    pointer_tok = 0;
  }
  if (global_cnt == 4) {
    Serial.println("Said: AT+INIT\n");
    miBT.println("AT+INIT\n");
    pointer_tok = 0;
  }
  if (global_cnt == 5) {
    Serial.println("Said: AT+INQM=1,3,4\n");
    miBT.println("AT+INQM=1,3,4\n");
    pointer_tok = 0;
  }
  if (global_cnt == 6) {
    global_dne = 1;
    global_cnt = 0;
    pointer_tok = 0;
    Timer1.initialize(400000);
  }
}
void _Config_() {
  pinMode(LED,OUTPUT);
  Serial.begin(9600);
  miBT.begin(38400);
  Timer1.initialize(100000);
  Timer1.attachInterrupt(ISR_Timer1);
  global_cfg = 1;
  
}
void ISR_Timer1() {
  
  global_cnt++;
  if (!global_dne) {
    _Donce_();
  }
  else {
      if (global_cnt >= 10) {
       global_cnt = 0;
       Serial.println("Said: AT+INQ\n");
       miBT.println("AT+INQ\n");
       pointer_tok = 0;
      }
  }
  if (cont_nodet >= CYCLES_ALARM*10 && global_dne == 1) {
    Serial.println("ND");
    Ala();
    noclose_flag = 1;
  }
  if (cont_nodet == 0 && interrupt_close == 0) {
    closeAla();
    noclose_flag = 0;
  }
  if (global_dne == 1) {
      cont_nodet++;
  }

  if (al_flag) {
    if (global_cnt%6 == 0) {
      digitalWrite(LED,HIGH);
      delay(6000);
      digitalWrite(LED,LOW);
      delay(6000);
      digitalWrite(LED,HIGH);
      delay(6000);
      digitalWrite(LED,LOW);
    }
  }
}
//-------------------------------------------------//
//    F U N C I O N E S :
//-------------------------------------------------//
int checkDist(char c0, char c1) {
 #ifdef ALARMA_LEJOS
  if (c0 != 'B' && c0 != 'C' && c0 != 'D' && c0 != 'E') {
    if (c0 == 'A') {
      if (c1 == 'F' || c1 == 'E' || c1 == 'D' || c1 == 'C' || c1 == 'B') {
        return 0;
      }
    }
    if (c0 == 'C') {
      if (c1 == '0' || c1 == '1' || c1 == '2' || c1 == '3' || c1 == '4') {
        return 0;
      }
    }
    return 1;
  }
  return 0;
  #endif
  #ifdef ALARMA_CERCA
  if (c0 != 'B' && c0 == 'A') {
    if (c0 == 'A') {
      if (c1 == 'F' || c1 == 'E' || c1 == 'D' || c1 == 'C' || c1 == 'B') {
        return 0;
      }
    }
    return 1;
  }
  return 0;
  #endif
}
int checkWord(char word1[],char word2[],int chars) {

  for (i = 0; i< chars; i++)
  {
    if (word1[i] != word2[i]) {
      return 0;
    }
  }
  return 1;
}
void printLine(char word1[], int len) {
  for (i = 0; i < len; i++) {
    Serial.write(word1[i]);
  }
  Serial.write("\n");
}
//-------------------------------------------------//
void setup(){
  rssi[0] = 'F';
  rssi[1] = 'F';
  rssi[2] = 'B';
  rssi[3] = '0';
  }
//-------------------------------------------------//
//    P R O G R A M A   M A I N :
//-------------------------------------------------//
void loop(){

  if ( !global_cfg ) {
    _Config_();
    Serial.println("Configurado");
  }
  
  if (miBT.available()) {
    global_char = miBT.read();
    Serial.write(global_char);
    
    if (rssi_flag) {
      rssi[4-rssi_flag] = global_char;
      rssi_flag--;
      Serial.print("\nFlag:");
      Serial.println(rssi_flag);
    }
    else {
      string[pointer_tok] = global_char;
    }
    
    pointer_tok++;
    
    if (global_char == '\n') {
      pointer_tok = 0;
      Serial.print("RSSI:");
      printLine(rssi,4);
      if (checkDist(rssi[2],rssi[3]) == 1) {
        Serial.println("Alarma por lejania/proximidad");
        Ala();
        interrupt_close = 1;
      }
      else {
        Serial.println("Cierre de alarma");
        if (noclose_flag == 0) {
          closeAla();
        }
        interrupt_close = 0;
      }
    }
  }

 
  if (checkWord(string,key,20) && pointer_tok == 21) {
     pointer_tok = 0;
     rssi_flag = 4;
     cont_nodet = 0;
     string[0] = 'X';
     Serial.write("\nDetectado:\n");
  }
}
