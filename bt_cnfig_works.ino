#include <SoftwareSerial.h>

SoftwareSerial miBT(10, 11);
// Pin 10 RX.
// Pin 11 TX.

void setup(){
  Serial.begin(9600);
  Serial.println("Listo");
  miBT.begin(38400); //La comunicación en modo configuración es SIEMPRE a 38400 baudios.
}

void loop(){
if (miBT.available())
   Serial.write(miBT.read());

if (Serial.available())
   miBT.write(Serial.read());
}
